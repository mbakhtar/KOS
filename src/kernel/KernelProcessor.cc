/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "kernel/AddressSpace.h"
#include "kernel/KernelProcessor.h"
#include "kernel/Thread.h"

inline void KernelProcessor::halt(bool deep) {
  DBG::outl(DBG::Idle, "entering ", (deep ? "deep" : "light"), " idle");
  stats->idle.count();
  ScopedLock<KernelLock> sl(haltLock);
  if (deep) preemptFlag = false;
  if (deep) MappedAPIC()->maskTimer();
  while (haltFlag) {
    haltLock.releaseHalt(); // might be woken up by other interrupts
    haltLock.acquire();     // -> need to check haltFlag again
  }
  haltFlag = true;
  if (deep) MappedAPIC()->unmaskTimer();
  if (deep) preemptFlag = true;
}

inline void KernelProcessor::idleLoop(bool irqs) {
#if TESTING_DISABLE_DEEP_IDLE
  irqs = true;
#endif
  cluster.addProcessor(*this);
  while (!terminate) {
    while (findWork());
    if (cluster.setProcessorIdle(*this, terminate)) {
      stats->idle.count();
      if (!CurrFM().zeroMemory<smallpl>()) halt(!irqs);
      cluster.setProcessorBusy(*this);
    }
  }
  unreachable();
}

void KernelProcessor::idleLoopSetup(KernelProcessor* p, bool irqs) {
  p->idleLoop(irqs);
}

void KernelProcessor::start(funcvoid0_t func, bool irqs) {
  kernelAS.initInvalidation(kernASM);
  defaultAS.initInvalidation(userASM);
  setupIdle(Thread::create(idleStackSize));
  idleStack->setup((ptr_t)idleLoopSetup, (ptr_t)this, (ptr_t)irqs);
  Thread* startThread = Thread::create(*this, defaultStackSize);
  HardwareProcessor::setupCurrStack(startThread);
  startThread->direct((ptr_t)func, _friend<KernelProcessor>());
}
