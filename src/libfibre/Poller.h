/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Poller_h_
#define _Poller_h_ 1

#if __FreeBSD__
#include <sys/event.h>
#else // __linux__ below
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#endif

class Fibre;
class PollerCluster;

class BasePoller {
protected:
  static const int maxPoll = 1024;
#if __FreeBSD__
  struct kevent events[maxPoll];
#else // __linux__ below
  epoll_event   events[maxPoll];
#endif
  int           pollFD;
  volatile bool pollTerminate;
  volatile bool paused;

  PollerStats *stats;

  template<typename T>
  static inline void pollLoop(T& This, bool pollerFibre);

public:
  BasePoller(const char* n = "BasePoller") : pollTerminate(false), paused(false) {
    stats = new PollerStats(this, n);
#if __FreeBSD__
    pollFD = SYSCALLIO(kqueue());
#else // __linux__ below
    pollFD = SYSCALLIO(epoll_create1(EPOLL_CLOEXEC));
#endif
  }
  ~BasePoller() {
    close(pollFD);
  }

  void pause() { paused = true; }

  template<bool ReadWrite = true>
  void registerFD(int fd) {
#if __FreeBSD__
    struct kevent ev[2];
    EV_SET(&ev[0], fd, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, 0);
    if (ReadWrite) EV_SET(&ev[1], fd, EVFILT_WRITE, EV_ADD | EV_CLEAR, 0, 0, 0);
    SYSCALL(kevent(pollFD, ev, ReadWrite ? 2 : 1, nullptr, 0, nullptr));
#else // __linux__ below
    epoll_event ev;
    // EPOLLERR, EPOLLHUP not actually needed?
    ev.events = EPOLLIN | EPOLLRDHUP | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLET;
    if (ReadWrite) ev.events |= EPOLLOUT;
    ev.data.fd = fd;
    SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_ADD, fd, &ev));
#endif
  }

  void deregisterFD(int fd) {
#if __FreeBSD__
    struct kevent ev[2];
    EV_SET(&ev[0], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
    EV_SET(&ev[1], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
    SYSCALL(kevent(pollFD, ev, 2, nullptr, 0, nullptr));
#else // __linux__ below
    SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_DEL, fd, nullptr));
#endif
  }
};

class PollerThread : public BasePoller {
  pthread_t pollThread;
  SystemSemaphore pauseSem;
protected:
  PollerThread(const char* n) : BasePoller(n) {}
  void start(void *(*loopSetup)(void*)) {
    SYSCALL(pthread_create(&pollThread, nullptr, loopSetup, this));
  }
public:
  ~PollerThread() { if (!pollTerminate) stop(); }
  pthread_t getSysID() { return pollThread; }
  void resume() { paused = false; pauseSem.V(); }
  inline int blockingPoll();
  void stop() {                    // use self-pipe trick to terminate poll loop
    pollTerminate = true;
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, 0, EVFILT_USER, EV_ADD | EV_ONESHOT, 0, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    EV_SET(&ev, 0, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    SYSCALL(pthread_join(pollThread, nullptr));
#else // __linux__ below
    int efd = SYSCALLIO(eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK));
    registerFD(efd);
    uint64_t val = 1;
    val = SYSCALL_EQ(write(efd, &val, sizeof(val)), sizeof(val));
    SYSCALL(pthread_join(pollThread, nullptr));
    SYSCALL(close(efd));
#endif
  }
};

class MasterPoller : public PollerThread {
  int timerFD;
  static void* pollLoopSetup(void*);
public:
  MasterPoller() : PollerThread("MasterPoller") {}
  ~MasterPoller() {
#if __linux__
    close(timerFD);
#endif
  }
  void start() { PollerThread::start(pollLoopSetup); }
  inline int blockingPoll();

  unsigned long initTimerHandling(unsigned long fd) {
#if __FreeBSD__
    timerFD = fd;
    fd += 1;
#else
    timerFD = SYSCALLIO(timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK | TFD_CLOEXEC));
    registerFD(timerFD);
#endif
    return fd;
  }

  void setTimer(const Time& reltimeout) {
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, timerFD, EVFILT_TIMER, EV_ADD | EV_ONESHOT, NOTE_USECONDS, reltimeout.toUS(), 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
#else
    itimerspec tval = { {0,0}, reltimeout };
    SYSCALL(timerfd_settime(timerFD, 0, &tval, nullptr));
#endif
  }

#if TESTING_POLLER_FIBRES
  template<bool Modify = false>
  void setupPollFD(int fd) { // (re)set up hierarchical pollling use ONESHOT
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, fd, EVFILT_READ, EV_ADD | EV_ONESHOT, 0, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
#else // __linux__ below
    epoll_event ev;
    ev.events = EPOLLIN | EPOLLONESHOT;
    ev.data.fd = fd;
    SYSCALL(epoll_ctl(pollFD, Modify ? EPOLL_CTL_MOD : EPOLL_CTL_ADD, fd, &ev));
#endif
  }
#endif
};

#if TESTING_POLLER_FIBRES

class Poller : public BasePoller {
  Fibre* pollFibre;
  FifoSemaphore<BinaryLock<>,true> pauseSem;
  static void pollLoopSetup(void*);
public:
  Poller(PollerCluster&);
  ~Poller() { if (!pollTerminate) stop(); }
  bool stopped() { return pollTerminate; }
  void stop();
  void resume() { paused = false; pauseSem.V(); }
  inline int blockingPoll();
};

#else

class Poller : public PollerThread {
  PollerCluster& cluster;
  static void* pollLoopSetup(void*);
public:
  Poller(PollerCluster& c) : PollerThread("PollerThread"), cluster(c) {
    PollerThread::start(pollLoopSetup);
  }
  inline int blockingPoll();
};

#endif

#endif /* _Poller_h_ */
