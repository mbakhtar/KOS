/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "runtime/RuntimeImpl.h"

// from background enqueue -> find idle "real" processor to run
void VirtualProcessor::wakeUp() {
  BaseProcessor* idleProc = cluster.findIdleProcessorHard();
  GENASSERT1(idleProc != this, FmtHex(this));
  if (idleProc) idleProc->wakeUp();
}

// any other enqueue -> wake idle processor, but don't try too hard
void VirtualProcessor::wakeCluster() {
  BaseProcessor* idleProc = cluster.findIdleProcessorSoft();
  if (idleProc && idleProc != this) idleProc->wakeUp();
}

bool BaseProcessor::findWork() {
#if TESTING_IDLE_SPIN
  static const size_t spinMax = TESTING_IDLE_SPIN;
#else
  static const size_t spinMax = 1;
#endif
  for (size_t spin = 0; spin < spinMax; spin += 1) {
    if (StackContext::idleYield(_friend<BaseProcessor>())) return true;
  }
  return false;
}

inline bool BaseProcessor::tryDequeue(StackContext*& s) {
#if TESTING_WORK_STEALING
  s = readyQueue.dequeueSafe();
#else
  s = readyQueue.dequeue();
#endif
  if fastpath(s) {
    stats->deq.count();
    return true;
  }
  return false;
}

inline bool BaseProcessor::tryStage(StackContext*& s) {
  s = cluster.stage();
  if slowpath(s) { // staging expected to happen rarely
    stats->stage.count();
    s->changeResumeProcessor(*this, _friend<BaseProcessor>());
    return true;
  }
  return false;
}

inline bool BaseProcessor::trySteal(StackContext*& s) {
#if TESTING_WORK_STEALING
  s = cluster.steal();
  if fastpath(s) {
#if TESTING_WORK_STEALING_STICKY
    static const size_t stickyStealThreshold = TESTING_WORK_STEALING_STICKY;
    if (s->getResumeProcessor().load() > stickyStealThreshold) {
      s->changeResumeProcessor(*this, _friend<BaseProcessor>());
      stats->move.count();
    } else
#endif
    stats->steal.count();
    return true;
  }
#endif
  return false;
}

inline bool BaseProcessor::tryBorrow(StackContext*& s) {
  s = cluster.borrow();
  if fastpath(s) {
    stats->borrow.count();
    return true;
  }
  return false;
}

StackContext* BaseProcessor::schedule(_friend<StackContext>) {
  StackContext* nextStack;
  if slowpath(terminate) {
    if fastpath(tryDequeue(nextStack)) return nextStack;
    return idleStack;
  }
#if TESTING_RESPONSIVE_FACTOR
  static const size_t responsiveFactor = TESTING_RESPONSIVE_FACTOR;
  if (++scheduleCounter >= responsiveFactor * cluster.procCount()) {
    scheduleCounter = 0;
    if slowpath(tryStage(nextStack)) return nextStack;
    if fastpath(tryBorrow(nextStack)) return nextStack;
    if fastpath(tryDequeue(nextStack)) return nextStack;
    if fastpath(trySteal(nextStack)) return nextStack;
    return idleStack;
  }
#endif
  if fastpath(tryDequeue(nextStack)) return nextStack;
  if slowpath(tryStage(nextStack)) return nextStack;
  if fastpath(trySteal(nextStack)) return nextStack;
  if fastpath(tryBorrow(nextStack)) return nextStack;
  return idleStack;
}
