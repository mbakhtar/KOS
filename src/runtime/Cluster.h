/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Cluster_h_
#define _Cluster_h_ 1

#include "runtime/BaseProcessor.h"

class Cluster {
#if TESTING_PLACEMENT_RR
  SystemLock       ringLock;
  BaseProcessor*   ringProc;
#endif
  volatile size_t  pCount;

  VirtualProcessor stagingProc;
  VirtualProcessor backgroundProc;

protected:
  SystemLock       idleLock;
  ProcessorList    idleList;
  volatile size_t  idleCount;
  ProcessorList    busyList;

public:
  Cluster() : pCount(0), stagingProc(*this, "Staging"), backgroundProc(*this, "Background"), idleCount(0) {
#if TESTING_PLACEMENT_RR
    ringProc = nullptr;
#endif
  }

  size_t procCount() const { return pCount; }

  void addProcessor(BaseProcessor& proc) {
#if TESTING_PLACEMENT_RR
    ScopedLock<SystemLock> slr(ringLock);
    if (ringProc == nullptr) {
      ProcessorRing::init(proc);
      ringProc = &proc;
    } else {
      ProcessorRing::insert_after(*ringProc, proc);
    }
#endif
    ScopedLock<SystemLock> sli(idleLock);
    busyList.push_back(proc);
    pCount += 1;
  }

  void removeProcessor(BaseProcessor& proc) {
#if TESTING_PLACEMENT_RR
    ScopedLock<SystemLock> slr(ringLock);
    GENASSERT(ringProc);
    // move ringProc, if necessary
    if (ringProc == &proc) ringProc = ProcessorRing::next(*ringProc);
    // ring empty?
    if (ringProc == &proc) ringProc = nullptr;
    ProcessorRing::remove(proc);
#endif
    ScopedLock<SystemLock> sli(idleLock);
    busyList.remove(proc);
    pCount -= 1;
  }

  VirtualProcessor& placement(_friend<StackContext>, bool bg = false, bool sg = false) {
    if slowpath(bg) return backgroundProc;
#if TESTING_PLACEMENT_RR
    if slowpath(sg) return stagingProc;
    ScopedLock<SystemLock> sl(ringLock);
    GENASSERT(ringProc);
    ringProc = ProcessorRing::next(*ringProc);
    return *ringProc;
#else
    return stagingProc;
#endif
  }

  StackContext* stage() {
    return stagingProc.dequeue(_friend<Cluster>());
  }

  StackContext* steal() {
    ScopedLock<SystemLock> sl(idleLock);
    BaseProcessor* p = busyList.front();
    while (p != busyList.edge()) {
      StackContext* s = p->dequeue<true>(_friend<Cluster>());
      if fastpath(s) return s;
      p = ProcessorList::next(*p);
    }
    return nullptr;
  }

  StackContext* borrow() {
    return backgroundProc.dequeue(_friend<Cluster>());
  }

  size_t setProcessorIdle(BaseProcessor& proc, bool terminating) {
    ScopedLock<SystemLock> sl(idleLock);
    if (!terminating && (backgroundProc.load() || stagingProc.load())) return 0;
    busyList.remove(proc);
    idleList.push_back(proc);
    idleCount += 1;
    Runtime::debugS("Processor ", FmtHex(&proc), " on idle list");
    return idleCount;
  }

  void setProcessorBusy(BaseProcessor& proc) {
    ScopedLock<SystemLock> sl(idleLock);
    idleCount -= 1;
    idleList.remove(proc);
    busyList.push_back(proc);
    Runtime::debugS("Processor ", FmtHex(&proc), " off idle list");
  }

  BaseProcessor* findIdleProcessorHard() {
    ScopedLock<SystemLock> sl(idleLock);
    return idleList.empty() ? nullptr : idleList.front();
  }

  BaseProcessor* findIdleProcessorSoft() {
    if (!idleCount) return nullptr;
    if (!idleLock.tryAcquire()) return nullptr;
    BaseProcessor* proc = idleList.empty() ? nullptr : idleList.front();
    idleLock.release();
    return proc;
  }
};

#endif /* _Cluster_h_ */
