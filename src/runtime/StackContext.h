/******************************************************************************
    Copyright � 2017 Martin Karsten

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _StackContext_h_
#define _StackContext_h_ 1

#include "generic/IntrusiveContainers.h" 
#include "runtime/Runtime.h"
#include "runtime/Stack.h"

#include <map>

static const size_t  topPriority = 0;
static const size_t  defPriority = 1;
static const size_t  lowPriority = 2;
static const size_t  maxPriority = 2;
static const size_t  numPriority = maxPriority + 1;
static const size_t idlePriority = numPriority;

class ResumeInfo;
class VirtualProcessor;
class BaseProcessor;
class Cluster;
class EventEngine;

#if TESTING_ENABLE_DEBUGGING
#define STACKLINKCOUNT 2
typedef IntrusiveList<StackContext,1,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> GlobalStackList;
extern SystemLock*      _globalStackLock;
extern GlobalStackList* _globalStackList;
#else
#define STACKLINKCOUNT 1
#endif

typedef IntrusiveList<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackList;
typedef IntrusiveQueue<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackQueue;
#if TESTING_NEMESIS_READYQUEUE
typedef IntrusiveQueueNemesis<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackMPSC;
typedef IntrusiveQueueNemesis<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> BlockingStackMPSC;
#else
typedef IntrusiveQueueStub<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>> StackMPSC;
typedef IntrusiveQueueStub<StackContext,0,STACKLINKCOUNT,DoubleLink<StackContext,STACKLINKCOUNT>,true> BlockingStackMPSC;
#endif

class StackContext : public StackList::Link {
  vaddr             stackPointer;    // holds stack pointer while stack inactive
  VirtualProcessor* resumeProcessor; // next resumption on this processor
  size_t            priority;        // scheduling priority
  size_t            balanceCounter;  // counting towards re-staging the stack
  bool              affinity;        // affinity prohibits re-staging

  enum SuspendState { Running, Prepared, Suspended };
  SuspendState      suspendState;
  ResumeInfo*       resumeInfo;      // race: unblock vs. timeout

  StackContext(const StackContext&) = delete;
  const StackContext& operator=(const StackContext&) = delete;

  // central stack switching routine
  enum SwitchCode { Idle = 'I', Migrate = 'M', Yield = 'Y', Preempt = 'P', Suspend = 'S', Terminate = 'T' };
  template<SwitchCode> inline bool switchStack();

  // these routines are called immediately after the stack switch
  static void postIdle     (StackContext* prevStack);
  static void postYield    (StackContext* prevStack);
  static void postPreempt  (StackContext* prevStack);
  static void postSuspend  (StackContext* prevStack);
  static void postTerminate(StackContext* prevStack);

  // Running -> Prepared; Prepared -> Suspended is attempted in postSuspend()
  void prepareSuspend() {
    GENASSERT1(suspendState == Running, FmtHex(this));
    __atomic_store_n( &suspendState, Prepared, __ATOMIC_RELAXED );
  }

  // non-static; restricted to ResumeInfo
  void suspend();

  // resumption internal interfaces
  void resumeInternal();

protected:
  // constructor/destructors can only be called by derived classes
  StackContext(VirtualProcessor& proc);    // main constructor
  StackContext(Cluster&, bool bg = false); // uses delegation
  ~StackContext() {
    GENASSERT1(suspendState == Running, FmtHex(this));
    GENASSERT1(resumeInfo == nullptr, FmtHex(this));
  }

  void initStackPointer(vaddr sp) {
    stackPointer = align_down(sp, stackAlignment);
  }

public:
  // direct switch to new stack
  void direct(ptr_t func, _friend<SystemProcessor>) __noreturn {
    stackDirect(stackPointer, func, nullptr, nullptr, nullptr);
  }

  // set up new stack
  void setup(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr) {
    stackPointer = stackInit(stackPointer, func, p1, p2, p3);
  }

  // set up new stack and resume for concurrent execution
  void start(ptr_t func, ptr_t p1 = nullptr, ptr_t p2 = nullptr, ptr_t p3 = nullptr) {
    setup(func, p1, p2, p3);
    resumeInternal();
  }

  // context switching interfaces; apply to CurrStack()
  static bool idleYield(_friend<BaseProcessor>);
  static void yield();
  static void preempt();
  static void terminate() __noreturn;

  void suspend(_friend<ResumeInfo>) { suspend(); }

  void prepareSuspend(_friend<ResumeInfo>) { return prepareSuspend(); }

  // Prepared/Suspended -> Running; resume stack later, if necessary
  template<bool ResumeNow=true>
  VirtualProcessor* resume() {
    SuspendState prevState = __atomic_exchange_n( &suspendState, Running, __ATOMIC_RELAXED );
    GENASSERT1(prevState != Running, FmtHex(this));
    if (prevState != Suspended) return nullptr;
    if (ResumeNow) resumeInternal();
    return resumeProcessor;
  }

  // set ResumeInfo with resumption information
  void setupResumeRace(ResumeInfo& ri, _friend<ResumeInfo>) {
    GENASSERT1(suspendState == Prepared, FmtHex(this));
    __atomic_store_n( &resumeInfo, &ri, __ATOMIC_RELAXED );
  }

  // race between different possible resumers -> winner cancels the others
  ResumeInfo* raceResume() {
    return __atomic_exchange_n( &resumeInfo, nullptr, __ATOMIC_RELAXED );
  }

  // set resume processor during scheduling
  void changeResumeProcessor(VirtualProcessor& rp, _friend<BaseProcessor>);
  VirtualProcessor& getResumeProcessor() { return *resumeProcessor; }

  // priority
  StackContext* setPriority(size_t p) { priority = p; return this; }
  size_t getPriority() const          { return priority; }

  // hard affinity - no staging
  bool getAffinity()       { return affinity; }
  void setAffinity(bool a) { affinity = a; }

  // migration
  static void              rebalance(size_t count = 1);
  static void              migrateSelf(Cluster&);
  static VirtualProcessor& migrateSelf(Cluster&, _friend<EventEngine>);        // Disk I/O
  static void              migrateSelf(VirtualProcessor&, _friend<EventEngine>); // Disk I/O
};

struct ResumeQueue {
  size_t count;
#if TESTING_CONCURRENT_READYQUEUE && TESTING_BLOCKING_CONCURRENT
  StackQueue queue;
  void push(StackContext& sc) { queue.push(sc); count += 1; }
#else
  StackQueue queue[numPriority];
  void push(StackContext& sc) { queue[sc.getPriority()].push(sc); count += 1; }
#endif
  ResumeQueue() : count(0) {}
};

typedef std::map<VirtualProcessor*,ResumeQueue> ProcessorResumeSet;

#endif /* _StackContext_h_ */
